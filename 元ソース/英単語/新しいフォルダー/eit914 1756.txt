grp	equ	0bfd0h	;ｸﾞﾗﾌｨｯｸ
msp	equ	0bff1h	;ﾒｯｾｰｼﾞ
khit	equ	0bcfdh	;ｳｪｲﾄ入力
khit2	equ	0be53h	;ﾉﾝｳｪｲﾄ入力
chrput	equ	0be62h	;ｷｬﾗｸﾀ配置
cursL	equ	0f0h	;curs x
cursH	equ	0f1h	;curs y
cursE	equ	0f2h	;curs最下点 5
cursD	equ	0f3h	;curs最上点 0
freeM	equ	0f4h


 org 100h

 call gre
 ld B,4
 ld D,0
 ld HL,titleg
l1:
 push BC
 ld E,4
 ld B,96
 call grp
 call timA
 inc HL
 inc D
 pop BC
 djnz l1
l2:
 ld DE,0405h
 ld HL,titlem
 ld B,14
 call msp
 call timr120
 cp 1eh
 jr z,main
 ld DE,0405h
 ld B,14
 inc HL
 call msp
 call timr120
 cp 1eh
 jr z,main
 jr l2
main:
 call gre
 ld DE,0003h
 ld B,11
 ld HL,manum
 call msp
 ld HL,manum2
 call mspmn
mncurs:
 ld HL,0205h
 ld (cursE),HL
 ld HL,0203h
 ld (cursL),HL
 ex DE,HL
 ld A,">"
 call chrput
mncurslp:
 call timr120
 call c,cursor
 cp 28h
 jr z,modesel
 call bookpr
 jr mncurslp
modesel:
 ld A,(cursH)
 cp 3
 jp c,anaume
 jp z,imiire
 cp 5
 jp c,zisho
 ret z
anaume:
 call datack












 ret
imiire:
 call datack










 ret
zisho:
 ld DE,(cursL)
 ld A," "
 call chrput
 ld A,2
 ld (cursH),A
 ld D,A
 ld A,">"
 call chrput
 ld HL,zishom
 call mspmn
zishol:
 call timr120
 call c,cursor
 cp 28h
 jr z,zishosel
 call bookpr
 jr zishol
zishosel:
 ld A,(cursH)
 cp 3
 jr c,miru
 jp z,kakikomu
 cp 5
 jp c,kesu
 jp main
miru:
 call datack



 ret
kakikomu:
 					;;;




 ret
kesu:
 call datack









 ret
zishom:
 db "ﾐﾙ  ","ｶｷｺﾑ","ｹｽ  ","ﾓﾄﾞﾙ"



bookpr:
 call rand4
 cp 1
 jr c,bookp1
 jr z,bookp2
 cp 3
 jr c,bookp3
 jr bookpr
bookp1:
 ld HL,book1
 jr bookp
bookp2:
 ld HL,book2
 jr bookp
bookp3:
 ld HL,book3
bookp:
 ld D,02
 ld B,3
bookpl:
 push BC
 ld E,11h
 ld B,24
 call grp
 inc HL
 inc D
 pop BC
 djnz bookpl
 ret

datack:
 ld HL,(dataqu)
 ld A,H
 or A
 ret nz
 ld A,L
 or A
 ret nz
 call gre
 ld HL,datackm
 ld DE,0106h
 ld B,11
 call msp
 call khit
 pop AF
 jp main

datackm:
 db "ﾃﾞｰﾀｶﾞｱﾘﾏｾﾝ"


mspmn:
 ld B,4
 ld D,02h
mainl1:
 push BC
 ld E,04h
 ld B,4
 call msp
 call timA
 inc D
 inc HL
 pop BC
 djnz mainl1
 ret


cursor:;(>)移動
 push AF
 ld DE,(cursL)
 ld A," "
 call chrput
 pop AF
 ex DE,HL
 cp 1fh	;down
 jr z,cursdw
 cp 20h	;up
 jr z,cursup
;cp 21	;left
;jr z,curslf
;cp 22	;right
;jr z,cursrg
 jr cursst
cursup:
 ld A,(cursD)
 cp H
 jr z,cursst
 dec H
 jr cursst
cursdw:
 ld A,(cursE)
 cp H
 jr z,cursst
 inc H
; jr cursst
cursst:
 push AF
 ld (cursL),HL
 ex DE,HL
 ld A,">"
 call chrput
 call timB
 pop AF
 ret


aschit:
 call khit2
 jr nc,aschit
 cp 18h
 jr nc,aschi2
 dec A
 dec A
 ld HL,ascd
 ld D,0
 ld E,A
 add HL,DE
 ld A,(HL)
 ret
aschi2:
 cp 28h
 jr z,ascENT
 cp 29h
 jr z,ascL
 cp 31h
 jr z,ascI
 cp 32h
 jr z,ascO
 cp 39h
 jr z,ascP
 cp 3ah
 jr z,ascBS
 xor A
 ret
ascENT:
 ld A,7bh
 ret
ascL:
 ld A,"l"
 ret
ascI:
 ld A,"i"
 ret
ascO:
 ld A,"o"
 ret
ascP:
 ld A,"p"
 ret
ascBS:
 ld A,7ch
 ret
ascd:
 db "qwertyuasdfghjkzxcvbnm"

rand:
 ld HL,0
 ld D,H
 ld E,L
 add HL,HL
 add HL,HL
 add HL,DE
 ld DE,6411H
 add HL,DE
 ld (rand+1),HL
 ld A,H
 ret
rand4:
 call rand
 and 00000111b
 ret

xsan:;A*B=HL
 push BC
 push DE
 ld E,A
 ld D,0
 ld HL,0
 or A
 jr z,xsanrt
 dec B
 jr z,xsanrt
 inc B
xsanl:
 add HL,DE
 djnz xsanl
xsanrt:
 pop DE
 pop BC
 ret



timC:
 ld A,120
 jr TIMER
timB:
 ld A,80
 jr TIMER
timA:
 ld A,40
TIMER:;A:ｼﾞｶﾝ
 push BC
 LD BC,0
T1:INC BC
 DB 0,0,0,0,0
 cp B
 jr z,tret
 JR T1
tret:
 pop BC
 ret
timr200:
 ld A,200
 jr timr
timr0:
 ld A,1
 jr timr
timr120:
 ld A,120
timr:;A:ｼﾞｶﾝ
 push BC
 ld B,A
T2:
 push BC
 call khit2
 pop BC
 jr c,tret
 or A	;cy reset
 dec B
 jr z,tret
 jr T2
gre:
 ld D,0
grelp:
 push DE
grel:
 ld B,12
 ld E,0
grel2:
 push BC
 push DE
 ld HL,space
 ld B,12
 call grp
 pop DE
 inc E
 inc E
 pop BC
 djnz grel2
 pop DE
 inc D
 ld A,D
 cp 6
 jr nz,grelp
 ret

manum:
 db "MODE SELECT";11
manum2:
 db "ｱﾅｳﾒ","ｲﾐｳﾒ","ｼﾞｼｮ","ｵﾜﾙ ";4*4
titlem:
 db "push space key";14
space:
 db 0,0,0,0,0,0,0,0,0,0,0,0,0,0
titleg:
 db 00,00,10h,30h,30h,30h,30h,30h,30h,30h,0FEh,0FEh,0FEh,30h,30h,30h,30h
 db 30h,30h,0FEh,0FEh,0FEh,30h,30h,30h,30h,30h,30h,30h,20h,00,00,00,00,00,00,00
 db 00,00,04,0Ch,3Ch,78h,70h,04,0Ch,3Ch,78h,70h,00,00,80h,0E0h,7Ch,3Ch,1Ch
 db 08,00,00,00,00,00,00,00,00,00,80h,98h,98h,98h,98h,98h,98h,98h,98h,88h
 db 80h,08,18h,18h,18h,0F8h,0F8h,0F8h,18h,18h,18h,98h,98h,1Ch,1Ch,18h,10h,00,00,00
 db 00,00,00,00,00,00,00,0F8h,0F8h,0F8h,19h,19h,19h,18h,18h,0FFh,0FFh,0FFh
 db 18h,19h,19h,19h,0FCh,0FCh,0F8h,10h,00,00,00,00,00,00,00,00,00,00,00,00
 db 0FFh,0FFh,0FFh,63h,63h,63h,63h,63h,0FFh,0FFh,0FFh,0FFh,63h,63h,63h,63h
 db 63h,0FFh,0FFh,0FEh,00,00,00,00,00,00,00,00,01,0D9h,0D9h,0D9h,0D9h,0D9h
 db 0D9h,0D9h,0D9h,49h,01,0C0h,0C3h,0C3h,0FFh,0FFh,0FFh,0C3h,0C3h,0C3h,0C3h
 db 0FFh,0FFh,0FFh,0C2h,0E0h,0E0h,0C0h,80h,00
 db 00,00,06,06,06,06,06,07,07,07,06,86h,0C6h,0F6h,0FEh,7Fh,3Fh,7Fh,0F6h,0E6h
 db 0C6h,86h,07,07,07,06,06,07,07,06,04,00,00,40h,0C0h,0C0h,0C0h,0C0h,0DFh,0DFh
 db 0CFh,0C6h,0C6h,0C6h,0C6h,0C6h,0FFh,0FFh,0FFh,0FFh,0C6h,0C6h,0C6h,0C6h,0C6h
 db 0CFh,0CFh,0CFh,0E0h,0E0h,0E0h,40h,00,00,00,00,00,0FCh,0FCh,0FCh,18h,18h,0F8h
 db 0FCh,0FCh,00,00,00,0FCh,0FCh,0FCh,18h,18h,18h,18h,18h,18h,18h,0FCh,0FCh,0F8h
 db 00,00,00,00,00
 db 00,00,10h,30h,30h,18h,18h,1Ch,0Ch,0Eh,07,07,03,01,00,00,00,00,00,01,03,07,0Fh
 db 0Fh,1Eh,1Eh,3Eh,3Ch,1Ch,0Ch,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
 db 3Fh,3Fh,3Fh,3Fh,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,1Fh,1Fh
 db 1Fh,06,06,1Fh,1Fh,1Fh,00,00,00,3Fh,3Fh,1Fh,06,06,06,06,06,06,06,1Fh,1Fh,1Fh
 db 00,00,00,00,00
book1:
 db 80h,40h,20h,20h,20h,20h,20h,20h,20h,20h,40h
 db 0C0h,0C0h,40h,20h,20h,20h,20h,20h,20h,20h,20h,40h,80h
 db 0FFh,00h,11h,44h,11h,04h,00h,00h,51h,04h,00h,0FFh
 db 0FFh,00,10h,45h,10h,44h,00,00,11h,44h,00h,0FFh
 db 3Fh,2Ch,34h,1Ah,16h,1Ah,16h,1Ah,16h,1Ah,34h,3Fh,3Fh
 db 34h,1Ah,16h,1Ah,16h,1Ah,16h,1Ah,34h,2Ch,3Fh
book2:
 db 80h,40h,20h,20h,20h,0FCh,04,0A8h,08h,50h,20h,0C0h
 db 0C0h,40h,20h,20h,20h,20h,20h,20h,20h,20h,40h,80h
 db 0FFh,00,11h,44h,11h,3Fh,40h,42h,90h,05h,00h,0FFh
 db 0FFh,00,10h,45h,10h,44h,00,00,11h,44h,00,0FFh
 db 3Fh,2Ch,34h,1Ah,16h,1Ah,16h,1Ah,16h,1Bh,36h,3Fh
 db 3Fh,34h,1Ah,16h,1Ah,16h,1Ah,16h,1Ah,34h,2Ch,3Fh
book3:
 db 80h,40h,20h,20h,20h,20h,20h,20h,20h,20h,40h,0C0h
 db 0C0h,20h,90h,08h,08h,24h,44h,0F8h,20h,20h,40h,80h
 db 0FFh,00h,11h,44h,11h,04h,00h,00h,51h,04h,00h,0FFh
 db 0FFh,00h,12h,80h,49h,42h,64h,3Fh,11h,44h,00h,0FFh
 db 3Fh,2Ch,34h,1Ah,16h,1Ah,16h,1Ah,16h,1Ah,34h,3Fh,3Fh
 db 37h,1Bh,16h,1Ah,16h,1Ah,16h,1Ah,34h,2Ch,3Fh




dataqu:;ﾃﾞｰﾀ総量 65535
 db 0,0
data:;やった回数,正解した回数,word,意味,0ffh,~~~~
 db 0

end